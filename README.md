# VAGRANT

Basique Vagrantfile pour instancier deux VM debian 12

## INSTALLATION VAGRANT

- Avec Virtualbox:
1. Installer virtualbox >= 7.0 (Debian 12)
2. S'assurer que Virtualbox est choisit comme "provider" dans le settings.yaml

- Sans Virtualbox:
```bash
sudo apt-get install vagrant-libvirt libvirt-daemon-system -y
sudo usermod --append --groups libvirt $USER
newgrp libvirt
```

## CONFIGURATION

La configuration s'éffectue dans le settings.yaml:
```yaml
vms:
  - name: "server-1" # nom vagrant
    hostname: "server" # nom réseau
    ip: "192.168.60.2" # IP
    memory: 1024 # RAM
    os: "debian/bookworm64" # OS
  - name: "vm-2"
    ...
```

> Les OS sont de la forme Vagrant boxes et sont régérencées ici: https://portal.cloud.hashicorp.com/vagrant/discover

## UTILISATION

- Construire les machines:
```bash
vagrant up
```

### AUTRES COMMANDES UTILES 

- Se connecter en SSH:
```bash
vagrant ssh <hostname>
```

- Stopper les machines:
```bash
vagrant halt
```

- Detruire les machines:
```bash
vagrant destroy
```

## DOCUMENTATION :books:

[Vagrant](https://developer.hashicorp.com/vagrant/docs)